<?php
return [
    'fromEmail'     => ['info@seges-electronics.ru' => 'SEGES-Electronics.ru'],
    'sellerEmail'   => 'sale@seges-electronics.ru',
    'testEmail'     => 'testcaseag1@gmail.com',
    'supportEmail'  => 'support@example.com',
    'user.passwordResetTokenExpire' => 14400,
];
