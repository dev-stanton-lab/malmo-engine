<?php

/** $var yii\web\View $this */

$url = \frontend\themes\base\assets\BaseAsset::register($this)->baseUrl;

?>

<div style="max-width: 1100px;">
    <div class="brendsconteiner">
        <h1>Hongfa</h1>
        <img src="<?=$url?>/img/hongfa.png" alt="hongfa" title="hongfa" width="166px" height="108px" />
        <p>Передовая компания-производитель электромагнитных и твердотельных реле, более 15 000 спецификаций реле. </p>
        <hr noshade size="1px" color="#ddd" />
        <a target="_blank" rel="nofollow"  href="http://www.hongfa.com/index.html"><div class="brendsbuttonsite">САЙТ КОМПАНИИ</div></a>
        <!--<a href=""><div class="brendsbuttonsite">ПРОДУКЦИЯ</div></a>-->
        <!--<a href=""><div class="brendsbuttonsite">ПОДРОБНЕЕ</div></a>-->
    </div>
    <div class="brendsconteiner">
        <h1>Raystar</h1>
        <img src="<?=$url?>/img/raystar.png" alt="raystar" title="raystar" width="166px" height="88px"/>
        <p>Компания, лидирующая в производстве высококлассных монохромных жидкокристаллических символьных и графических дисплеев (LCD), модулей OLED (OrganicLightEmittingDiode), цветных TFT индикаторов, индикаторов заказного типа (COG)                    </p>
        <hr noshade size="1px" color="#ddd" />
        <a target="_blank" rel="nofollow"  rel="nofollow" href="http://www.raystar-optronics.com/index.php?lang=ru"><div class="brendsbuttonsite">САЙТ КОМПАНИИ</div></a>
        <!--<a href=""><div class="brendsbuttonsite">ПРОДУКЦИЯ</div></a>-->
        <!--<a href=""><div class="brendsbuttonsite">ПОДРОБНЕЕ</div></a>-->
    </div>

    <div class="brendsconteiner">
        <h1>Fuzetec</h1>
        <img src="<?=$url?>/img/fuzetec.png" alt="fuzetec" title="fuzetec" width="166px" height="109px"/>
        <p>Компания – лидер на рынке технологий защиты электрических цепей, выпускающая широкое разнообразие самовосстанавливающих полимерных предохранителей Polyswitch. Обеспечивает решения для продолжительной защиты от коротких замыканий для настоящего и будущего развития электронной и электротехнической промышленности.</p>
        <hr noshade size="1px" color="#ddd" />
        <a target="_blank" rel="nofollow"  href="http://www.fuzetec.com/"><div class="brendsbuttonsite">САЙТ КОМПАНИИ</div></a>
        <!--<a href=""><div class="brendsbuttonsite">ПРОДУКЦИЯ</div></a>-->
        <!--<a href=""><div class="brendsbuttonsite">ПОДРОБНЕЕ</div></a>-->
    </div>

    <div class="brendsconteiner">
        <h1>SJK</h1>
        <img src="<?=$url?>/img/sjk.png" alt="sjk" title="sjk" width="166px" height="109px"/>
        <p>Компания, производящая лучшие на сегодняшний день кварцевые резонаторы и генераторы. Высокая добротность, температурная и временная стабильностью, низкий уровень фазовых шумов – отличительные особенности продукции SJK.</p>
        <hr noshade size="1px" color="#ddd" />
        <a target="_blank" rel="nofollow"  href="http://www.q-crystal.com/"><div class="brendsbuttonsite">САЙТ КОМПАНИИ</div></a>
        <!--<a href=""><div class="brendsbuttonsite">ПРОДУКЦИЯ</div></a>-->
        <!--<a href=""><div class="brendsbuttonsite">ПОДРОБНЕЕ</div></a>-->
    </div>

    <div class="brendsconteiner">
        <h1>Trxcom</h1>
        <img src="<?=$url?>/img/trxcom.png" alt="trxcom" title="trxcom" width="166px" height="109px"/>
        <p>Специализированный производитель высококачественных разъемов RJ-45 со встроенными трансформаторами и различных индуктивностей.</p>
        <hr noshade size="1px" color="#ddd" />
        <a target="_blank" rel="nofollow"  href="http://www.trxcom.com/"><div class="brendsbuttonsite">САЙТ КОМПАНИИ</div></a>
        <!--<a href=""><div class="brendsbuttonsite">ПРОДУКЦИЯ</div></a>-->
        <!--<a href=""><div class="brendsbuttonsite">ПОДРОБНЕЕ</div></a>-->
    </div>

    <div class="brendsconteiner">
        <h1>Xtd</h1>
        <img src="<?=$url?>/img/xtd.png" alt="xtd" title="xtd" width="166px" height="109px"/>
        <p>Одна из немногих компаний специализирующихся на производстве высококачественных литиевых аккумуляторов, которые обеспечат стабильность и продолжительный период работы устройств требующих автономное питание.</p>
        <hr noshade size="1px" color="#ddd" />
        <a target="_blank" rel="nofollow"  href="http://www.xktd.com/"><div class="brendsbuttonsite">САЙТ КОМПАНИИ</div></a>
        <!--<a href=""><div class="brendsbuttonsite">ПРОДУКЦИЯ</div></a>-->
        <!--<a href=""><div class="brendsbuttonsite">ПОДРОБНЕЕ</div></a>-->
    </div>

    <div class="brendsconteiner">
        <h1>Rayex</h1>
        <img src="<?=$url?>/img/rayex.png" alt="rayex" title="rayex" width="166px" height="109px"/>
        <p>Компания, зарекомендовавшая себя как первоклассный производитель электромеханических реле различной мощности и назначения. Отменное сочетание стоимости и качества продукции – по праву заслуживают свое место в передовых разработках.</p>
        <hr noshade size="1px" color="#ddd" />
        <a target="_blank" rel="nofollow"  href="http://www.relay-rayex.com"><div class="brendsbuttonsite">САЙТ КОМПАНИИ</div></a>
        <!--<a href=""><div class="brendsbuttonsite">ПРОДУКЦИЯ</div></a>-->
        <!--<a href=""><div class="brendsbuttonsite">ПОДРОБНЕЕ</div></a>-->
    </div>
    <div class="brendsconteiner">
        <h1>Aplus</h1>
        <img src="<?=$url?>/img/aplus.png" alt="aplus" title="aplus" width="166px" height="109px"/>
        <p>
            Уникальная в своем направлении компания, производящая микросхемы для записи и воспроизведения звуковых данных. Качество звучания и легкость в использовании – те качества, которые ищет практически любой разработчик.
        </p>
        <hr noshade size="1px" color="#ddd" />
        <a target="_blank" rel="nofollow"  href="http://www.aplusinc.com.tw/"><div class="brendsbuttonsite">САЙТ КОМПАНИИ</div></a>
        <!--<a href=""><div class="brendsbuttonsite">ПРОДУКЦИЯ</div></a>-->
        <!--<a href=""><div class="brendsbuttonsite">ПОДРОБНЕЕ</div></a>-->
    </div>

    <div class="brendsconteiner">
        <h1>Astron</h1>
        <img src="<?=$url?>/img/astron.png" alt="astron" title="astron" width="166px" height="109px"/>
        <p>Без лишней скромности, известный и популярный производитель держателей SIM-карт, SD-карт, различных разъёмов и коннекторов. Продукция Astron по праву занимает свое место в передовых электронных устройствах совместно с GSM, CDMA M2M модулями. </p>
        <hr noshade size="1px" color="#ddd" />
        <a target="_blank" rel="nofollow"  href="http://www.astron.com.tw/"><div class="brendsbuttonsite">САЙТ КОМПАНИИ</div></a>
        <!--<a href=""><div class="brendsbuttonsite">ПРОДУКЦИЯ</div></a>-->
        <!--<a href=""><div class="brendsbuttonsite">ПОДРОБНЕЕ</div></a>-->
    </div>
    <div class="brendsconteiner">
        <h1>Fuantronics</h1>
        <img src="<?=$url?>/img/fuantronics.png" alt="fuantronics" title="fuantronics" width="166px" height="109px"/>
        <p>Один из крупнейших производителей трансформаторов, дросселей, различных индуктивностей, каркасов и сердечников из ферримагнитных материалов, входящий в группу компаний Pairui. Сосредотачивает в себе передовые технологии производства, высокое качество продукции и неизменно стабильные конкурентные цены.</p>
        <hr noshade size="1px" color="#ddd" />
        <a target="_blank" rel="nofollow"  href="http://www.fuantronics.net/"><div class="brendsbuttonsite">САЙТ КОМПАНИИ</div></a>
        <!--<a href=""><div class="brendsbuttonsite">ПРОДУКЦИЯ</div></a>-->
        <!--<a href=""><div class="brendsbuttonsite">ПОДРОБНЕЕ</div></a>-->
    </div>

    <div class="brendsconteiner">
        <h1>RFID</h1>
        <img src="<?=$url?>/img/rfid.png" alt="rfid" title="rfid" width="166px" height="109px"/>
        <p>RFID Бренд, объединяющий в себе группу компаний производителей различных компонентов систем радио частотной идентификации. Широкий ассортимент устройств считывания и записи, транспондеров идентификации различных конфигураций и стандартов (Em-Marine 125КГц, Mifare 13,56 MГц, ICode 13.56 МГц, UHF). Карты, объединяющие в себе технологии сохранения информации на магнитной полосе LoCo или HiCo и технологии бесконтактной идентификации RFID (Radio Frequency IDentification).</p>
        <hr noshade size="1px" color="#ddd" />
        <a target="_blank" rel="nofollow"  href=""><div class="brendsbuttonsite">САЙТ КОМПАНИИ</div></a>
        <!--<a href=""><div class="brendsbuttonsite">ПРОДУКЦИЯ</div></a>-->
        <!--<a href=""><div class="brendsbuttonsite">ПОДРОБНЕЕ</div></a>-->
    </div>

    <div class="brendsconteiner">
        <h1>Smart</h1>
        <img src="<?=$url?>/img/smart.png" alt="smart" title="smart" width="166px" height="109px"/>
        <p>С высокой производительности, высокого качества, конкурентные цены и отличное техническое обслуживание, Hongfa реле является идеальным выбором для наших клиентов. Мы с нетерпением ждем растут и процветают со всеми нашими партнерами и клиентами по всему миру. </p>
        <hr noshade size="1px" color="#ddd" />
        <a target="_blank" rel="nofollow"  href="http://www.modulestek.com/"><div class="brendsbuttonsite">САЙТ КОМПАНИИ</div></a>
        <!--<a href=""><div class="brendsbuttonsite">ПРОДУКЦИЯ</div></a>-->
        <!--<a href=""><div class="brendsbuttonsite">ПОДРОБНЕЕ</div></a>-->
    </div>

    <div class="brendsconteiner">
        <h1>Nengshi</h1>
        <img src="<?=$url?>/img/nengshi.png" alt="nengshi" title="nengshi" width="166px" height="109px"/>
        <p>Nengshi Компания, специализирующаяся на производстве компонентов для проводных коммуникаций – кроссовое оборудование, инструмент, элементы и модули грозозащиты.</p>
        <hr noshade size="1px" color="#ddd" />
        <a target="_blank" rel="nofollow"  href="http://www.nenshi.com/"><div class="brendsbuttonsite">САЙТ КОМПАНИИ</div></a>
        <!--<a href=""><div class="brendsbuttonsite">ПРОДУКЦИЯ</div></a>-->
        <!--<a href=""><div class="brendsbuttonsite">ПОДРОБНЕЕ</div></a>-->
    </div>

    <div class="brendsconteiner">
        <h1>Nuvoton</h1>
        <img src="<?=$url?>/img/nuvoton.png" alt="nuvoton" title="nuvoton" width="166px" height="109px"/>
        <p>Компания с мировым именем, занимающая лидирующие позиции в производстве передовых микроконтроллеров на базе ядра ARM. Высокая производительность и богатый набор периферийных модулей, сверхмалое собственное потребление – те важные качества микроконтроллеров Nuvoton, которые выбирают мировые производители электронных устройств.</p>
        <hr noshade size="1px" color="#ddd" />
        <a target="_blank" rel="nofollow"  href="http://www.nuvoton.com/hq/enu/pages/default.aspx"><div class="brendsbuttonsite">САЙТ КОМПАНИИ</div></a>
        <!--<a href=""><div class="brendsbuttonsite">ПРОДУКЦИЯ</div></a>-->
        <!--<a href=""><div class="brendsbuttonsite">ПОДРОБНЕЕ</div></a>-->
    </div>
</div>