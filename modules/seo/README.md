# создание seo models

1. seo модель привязывается к любой другой active record модели
2. модель имеет параметры для матчинга - route параметры:
    - route и routeParams.
      модель может иметь один и более route которые она обслуживает
      routeParams определяются как публичные свойства в модели и в функции routeAttributes должны возвращаться
      названия данных переменных, а в функции routeRules правила валидации
    - metaAttributes - дополнительные аттрибуты данного правила
      определяются в двух функциях: metaAttributes и metaRules назначение аналогично route функциям
    - модель должна иметь свой view файл, который задается в функции viewFile