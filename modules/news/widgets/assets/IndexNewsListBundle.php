<?php


namespace news\widgets\assets;

use yii\web\AssetBundle;

/**
 * Assets for all widgets
 *
 * @package news\widgets
 */
class IndexNewsListBundle extends AssetBundle
{
    public $sourcePath = '@news/widgets/assets/index-news-tiles';

}